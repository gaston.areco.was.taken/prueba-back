package ar.edu.teclab.prueba.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.edu.teclab.prueba.dto.CareerDTO;
import ar.edu.teclab.prueba.models.Career;
import ar.edu.teclab.prueba.service.implementations.CareerService;
import ar.edu.teclab.prueba.service.implementations.CourseService;
import ar.edu.teclab.prueba.service.implementations.StudentService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import ar.edu.teclab.prueba.dto.Ejemplo;
import ar.edu.teclab.prueba.dto.CommentDTO;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/test")
@CrossOrigin(origins = "*")
public class PruebaController {

	private static final Log LOG = LogFactory.getLog(PruebaController.class);

	@Autowired
	protected CareerService careerService;

	@Autowired
	protected StudentService studentService;

	@Autowired
	protected CourseService courseService;

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/ejemplo")
	public ResponseEntity<Ejemplo> getMessageStatus(@RequestParam(value ="nombre") String nombre) {

		try {

			Ejemplo ejemplo = new Ejemplo();
			ejemplo.setNombre(nombre);

			return ResponseEntity.ok(ejemplo);

		} catch (Exception e){

			LOG.error("Error", e);

		}

		return null;

	}

	@GetMapping("/listaComentarios")
	public Map getCommentsList(@RequestParam(value ="ticketID") String ticketID) {

		try {

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization","Basic am9yZ2UuZGFubmlAdGVjbGFiLmVkdS5hcjpBYnJpbDIwMTk=");
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		HttpEntity<String> entity = new HttpEntity<String>(headers);

		return restTemplate.exchange("https://teclab1593636133.zendesk.com/api/v2/tickets/" + ticketID + "/comments.json", HttpMethod.GET, entity, Map.class).getBody();

		} catch (Exception e){

			LOG.error("Error", e);

		}

		return null;

	}

	@PutMapping("/insertarComentario")
	public Map putComment(@RequestParam(value ="ticketID") String ticketID, @RequestBody CommentDTO commentDTO) {

		try {

			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization","Basic am9yZ2UuZGFubmlAdGVjbGFiLmVkdS5hcjpBYnJpbDIwMTk=");
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

			Map<String, Object> nestedParam = new HashMap<String, Object>();
			nestedParam.put("comment", commentDTO);
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("ticket",nestedParam);

			HttpEntity<Map> entity = new HttpEntity<Map>(param,headers);

			return restTemplate.exchange("https://teclab1593636133.zendesk.com/api/v2/tickets/" + ticketID + ".json", HttpMethod.PUT, entity, Map.class).getBody();

		} catch (Exception e){

			LOG.error("Error", e);

		}

		return null;

	}

	@GetMapping(value="/api/careers")
	public Map careers() {

		Map<String, Object> result = new HashMap<>();

		List<Career> list = careerService.findAll();

		result.put("timestamp", System.currentTimeMillis());
		result.put("status", list.size() > 0 ? 200 : 404);
		result.put("message", list.size() > 0 ? "Data found" : "Empty response");
		result.put("data", list);

		return result;

	}

	@GetMapping(value = "/api/careers/{id}")
	public Map careersById(@PathVariable("id") Long id) {

		Map<String, Object> result = new HashMap<>();

		Career oneCareer = careerService.findOne(id);

		result.put("timestamp", System.currentTimeMillis());
		result.put("status", oneCareer != null ? 200 : 404);
		result.put("data", oneCareer != null ? oneCareer : "Not found");

		return result;

	}

	@PostMapping(value="/api/careers")
	public Map createCareer(@RequestBody CareerDTO careerDTO) {

		Career career = new Career();

		career.setName(careerDTO.getName());
		career.setDescription(careerDTO.getDescription());
		career.setDuration(careerDTO.getDuration());
		career.setStudent(studentService.findOne(careerDTO.getIdStudent()));
		career.setCourse(courseService.findOne(careerDTO.getIdCourse()));

		this.careerService.save(career);

		Map<String, Object> result = new HashMap<>();

		result.put("timestamp", System.currentTimeMillis());
		result.put("status", career.equals(null) ? 404 : 200);
		result.put("data", career);

		return result;

	}

	@DeleteMapping(value = "api/careers/{id}")
	public void deleteCareer(@PathVariable Long id) {

		careerService.delete(id);

	}

}

