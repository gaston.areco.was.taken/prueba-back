package ar.edu.teclab.prueba.dto;

import java.io.Serializable;

public class CommentDTO implements Serializable {

    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
