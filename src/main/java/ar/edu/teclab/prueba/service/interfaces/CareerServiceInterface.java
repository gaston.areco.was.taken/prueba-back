package ar.edu.teclab.prueba.service.interfaces;

import ar.edu.teclab.prueba.models.Career;

import java.util.List;

public interface CareerServiceInterface {

    public Career findOne(Long id);
    public List<Career> findAll();
    public Career save(Career career);
    public void delete(Long id);

}
