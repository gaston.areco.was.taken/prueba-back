package ar.edu.teclab.prueba.repositories;

import ar.edu.teclab.prueba.models.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long> {
}
