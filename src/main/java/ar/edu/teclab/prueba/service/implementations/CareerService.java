package ar.edu.teclab.prueba.service.implementations;

import ar.edu.teclab.prueba.models.Career;
import ar.edu.teclab.prueba.repositories.CareerRepository;
import ar.edu.teclab.prueba.service.interfaces.CareerServiceInterface;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CareerService implements CareerServiceInterface {

    @Autowired
    private CareerRepository careerRepository;

    @Override
    public Career findOne(Long id) {

        return this.careerRepository.findOne(id);
    }

    @Override
    public List<Career> findAll() {

        return Lists.newArrayList(this.careerRepository.findAll());

    }

    @Override
    public Career save(Career career) {

        return this.careerRepository.save(career);

    }

    @Override
    public void delete(Long id) {

        this.careerRepository.delete(id);

    }

}
