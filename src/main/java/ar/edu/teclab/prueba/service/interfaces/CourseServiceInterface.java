package ar.edu.teclab.prueba.service.interfaces;

import ar.edu.teclab.prueba.models.Course;

public interface CourseServiceInterface {

    public Course findOne(Long id);

}
