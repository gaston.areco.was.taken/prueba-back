package ar.edu.teclab.prueba.service.interfaces;

import ar.edu.teclab.prueba.models.Student;

public interface StudentServiceInterface {

    public Student findOne(Long id);

}
