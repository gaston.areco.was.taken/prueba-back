package ar.edu.teclab.prueba.service.implementations;

import ar.edu.teclab.prueba.models.Course;
import ar.edu.teclab.prueba.repositories.CourseRepository;
import ar.edu.teclab.prueba.service.interfaces.CourseServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CourseService implements CourseServiceInterface {

    @Autowired
    private CourseRepository courseRepository;

    @Override
    public Course findOne(Long id) {

        return this.courseRepository.findOne(id);
    }

}
