package ar.edu.teclab.prueba.repositories;

import ar.edu.teclab.prueba.models.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
}
