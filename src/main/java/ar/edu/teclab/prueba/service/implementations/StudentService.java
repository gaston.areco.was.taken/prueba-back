package ar.edu.teclab.prueba.service.implementations;

import ar.edu.teclab.prueba.models.Student;
import ar.edu.teclab.prueba.repositories.StudentRepository;
import ar.edu.teclab.prueba.service.interfaces.StudentServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService implements StudentServiceInterface {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Student findOne(Long id) {

        return this.studentRepository.findOne(id);
    }

}
