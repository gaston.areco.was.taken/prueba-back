package ar.edu.teclab.prueba.repositories;

import ar.edu.teclab.prueba.models.Career;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CareerRepository extends CrudRepository<Career, Long> {
}
